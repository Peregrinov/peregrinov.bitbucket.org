
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  ehrId = "";
  sessionId = getSessionId();
  jQuery('#ustvarjenId').html('');
  // TODO: Potrebno implementirati
  
   if (stPacienta == 1) {
    var ime = "Silvo";
    var priimek = "Dasher";
    var datumRojstva = "1968-12-13T11:40Z";
    var merilec = "Merilec";

    var datumInUraSeznam = ["2000-06-08","2003-04-04","2007-09-01","2007-12-03","2009-10-10","2009-10-11","2009-10-12","2013-02-03","2014-01-01","2016-07-09","2018-01-01"];
    var telesnaVisina = "179";
    var telesnaTezaSeznam = ["85","78","70","74","76","89","83","81","85","100","85"];
  }
  else if (stPacienta == 2) {
    var ime = "Pavl";
    var priimek = "Tun";
    var datumRojstva = "2000-10-15T15:20Z";
    var merilec = "Merilec";

    var datumInUraSeznam = ["2000-06-08","2003-04-04","2007-09-01","2007-12-03","2009-10-10","2009-10-11","2009-10-12","2013-02-03","2014-01-01","2016-07-09","2018-01-01"];
    var telesnaVisina = "160";
    var telesnaTezaSeznam = ["60","59","55","51","54","56","59","62","66","59","54"];
    var merilec = "Merilec";
  }
  else {
    var ime = "Natasa";
    var priimek = "Macek";
    var datumRojstva = "1990-05-21T09:35Z";

    var datumInUraSeznam = ["2000-06-08","2003-04-04","2007-09-01","2007-12-03","2009-10-10","2009-10-11","2009-10-12","2013-02-03","2014-01-01","2016-07-09","2018-01-01"];
    var telesnaVisina = "185";
    var telesnaTezaSeznam = ["78","86","91","84","77","70","85","81","79","77","78"];
    
  var merilec = "Merilec";
  }

$.ajaxSetup({
        headers: {"Ehr-Session": sessionId}
    });
    $.ajax({
        url: baseUrl + "/ehr",
        type: 'POST',
        headers: {
        "Authorization": getAuthorization()
      },
        success: function (data) {
            var ehrId = data.ehrId;
            var partyData = {
                firstNames: ime,
                lastNames: priimek,
                dateOfBirth: datumRojstva,
                additionalInfo: {"ehrId": ehrId}
            };
            $.ajax({
                url: baseUrl + "/demographics/party",
                type: 'POST',
                headers: {
            "Authorization": getAuthorization()
          },
                contentType: 'application/json',
                data: JSON.stringify(partyData),
                success: function (party) {
                    if (party.action == 'CREATE') {
                      var element = document.getElementById("ustvarjenId");
                      element.insertAdjacentHTML('beforeend', "<span class='obvestilo " +
                          "label label-success fade-in'>"+ ime + " " + priimek + ": " +
                          ehrId + "</span><br>");
                      for (var i = 0; i < 11; i++) {
                        var datumInUra = datumInUraSeznam[i];
                      var telesnaTeza = telesnaTezaSeznam[i];
                        $("#preberiEHRid").val(ehrId);
    var podatki = {
        "ctx/language": "en",
        "ctx/territory": "SI",
        "ctx/time": datumInUra,
        "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
        "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
    };
    var parametriZahteve = {
        ehrId: ehrId,
        templateId: 'Vital Signs',
        format: 'FLAT',
        committer: merilec
    };
    $.ajax({
        url: baseUrl + "/composition?" + $.param(parametriZahteve),
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(podatki),
        headers: {
        "Authorization": getAuthorization()
      },
        success: function (res) {
        },
        error: function(err) {
          $("#dodajMeritveVitalnihZnakovSporocilo").html(
          "<span class='obvestilo label label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
        }
    });
                      }
                      return ehrId;
                    }
                },
                error: function(err) {
                  $("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
                }
            });
        }
    });
  return true;
 }


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
function kreirajEHRzaBolnika() {
	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
  var datumRojstva = $("#kreirajDatumRojstva").val();

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId}
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              $("#kreirajSporocilo").html("<span class='obvestilo " +
                "label label-success fade-in'>Uspešno kreiran EHR '" +
                ehrId + "'.</span>");
              $("#preberiEHRid").val(ehrId);
            }
          },
          error: function(err) {
          	$("#kreirajSporocilo").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
        });
      }
		});
	}
}

function dodajMeritveVitalnihZnakov() {
	var ehrId = $("#dodajVitalnoEHR").val();
	var datumInUra = $("#dodajVitalnoDatumInUra").val();
	var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
	var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
	var merilec = $("#dodajVitalnoMerilec").val();
	 var totalBMI = telesnaTeza/(telesnaVisina*telesnaVisina/10000);

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (res) {
        $("#dodajMeritveVitalnihZnakovSporocilo").html(
          "<span class='obvestilo label label-success fade-in'>" +
          "Vaš BMI je: " +  totalBMI + ".</span>");
      },
      error: function(err) {
      	$("#dodajMeritveVitalnihZnakovSporocilo").html(
          "<span class='obvestilo label label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
      }
		});
	}
}

function preberiMeritveVitalnihZnakov() {
	var ehrId = $("#meritveVitalnihZnakovEHRid").val();
	var tip = $("#preberiTipZaVitalneZnake").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {
          "Authorization": getAuthorization()
        },
	    	success: function (data) {
  				var party = data.party;
  				$("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
            "podatkov za <b>'" + tip + "'</b> bolnika <b>'" + party.firstNames +
            " " + party.lastNames + "'</b>.</span><br/><br/>");
  				if (tip == "telesna temperatura") {
  					$.ajax({
    				  url: baseUrl + "/view/" + ehrId + "/" + "body_temperature",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			    	if (res.length > 0) {
    				    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna temperatura</th></tr>";
  				        for (var i in res) {
    		            results += "<tr><td>" + res[i].time +
                      "</td><td class='text-right'>" + res[i].temperature +
                      " " + res[i].unit + "</td></tr>";
  				        }
  				        results += "</table>";
  				        $("#rezultatMeritveVitalnihZnakov").append(results);
    			    	} else {
    			    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
    			    	}
    			    },
    			    error: function() {
    			    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
    			    }
  					});
  				} else if (tip == "telesna teža") {
  					$.ajax({
    			    url: baseUrl + "/view/" + ehrId + "/" + "weight",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			    	if (res.length > 0) {
    				    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna teža</th></tr>";
  				        for (var i in res) {
    		            results += "<tr><td>" + res[i].time +
                      "</td><td class='text-right'>" + res[i].weight + " " 	+
                      res[i].unit + "</td></tr>";
  				        }
  				        results += "</table>";
  				        $("#rezultatMeritveVitalnihZnakov").append(results);
    			    	} else {
    			    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
    			    	}
    			    },
    			    error: function() {
    			    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
    			    }
  					});
  				}
	    	},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}
}


$(document).ready(function() {

var baseUrl1 = "https://teaching.lavbic.net/OIS/chat/api";
  var user = {id: 33, name: "Mojca Marjetica"}; // TODO: vnesi svoje podatke
  var nextMessageId = 0;
  var currentRoom = "Skedenj";


  // Naloži seznam sob
  $.ajax({
    url: baseUrl1 + "/rooms",
    type: "GET",
    success: function (data) {
      for (var i in data) {
        $("#rooms").append(" \
          <li class='media'> \
            <div class='media-body room' style='cursor: pointer;'> \
              <div class='media'> \
                <a class='pull-left' href='#'> \
                  <img class='media-object img-circle' src='img/" + data[i] + ".jpg' /> \
                </a> \
                <div class='media-body'> \
                  <h5>" + data[i] + "</h5> \
                </div> \
              </div> \
            </div> \
          </li>");
      }
      $(".room").click(changeRoom);
    }
  });


  // TODO: Naloga
  // Definicija funkcije za pridobivanje pogovorov, ki se samodejno ponavlja na 5 sekund
   var updateChat = function() {
    $.ajax({
      url: baseUrl1 + "/messages/" + currentRoom + "/" + nextMessageId,
      type: "GET",
       success: function (data) {
        for (var i in data) {
          var message = data[i];
          $("#messages").append(" \
            <li class='media'> \
              <div class='media-body'> \
                <div class='media'> \
                  <a class='pull-left'href='#'> \
                    <img class='media-object img-circle' src='https://randomuser.me/api/portraits/men/" + message.user.id + ".jpg'> \
                  </a> \
                  <div class='media-body'> \
                    <small class='text-muted'>" + message.user.name + " | " + message.time+ "</small> <br> \
                        " + message.text+ " \
                    <hr> \
                  </div> \
                </div> \
              </div> \
            </li>");
          nextMessageId = message.id+ 1;
        }
        setTimeout(function() {updateChat()} ,5000);
      }
    });
  };

  // Klic funkcije za začetek nalaganja pogovorov


  // TODO: Naloga
  // Definicija funkcije za posodabljanje seznama uporabnikov, ki se samodejno ponavlja na 5 sekund
  var updateUsers = function() {
    $.ajax({
      url: baseUrl1 + "/users/" + currentRoom,
      type: "GET",
      success: function (data) {
        $("#users").html("");
        for (var i in data) {
          var user = data[i];
          $("#users").append(" \
            <li class='media'> \
              <div class='media-body'> \
                <div class='media'> \
                  <a class='pull-left' href='#'> \
                    <img class='media-object img-circle' src='https://randomuser.me/api/portraits/men/" + user.id + ".jpg' /> \
                  </a> \
                  <div class='media-body' > \
                    <h5>" + user.name + "</h5> \
                  </div> \
                </div> \
              </div> \
            </li>");
        }
        setTimeout(function() {updateUsers()} , 5000);
      }
    });
  };


  // Klic funkcije za začetek posodabljanja uporabnikov
 updateUsers();

  // TODO: Naloga
  // Definicija funkcije za pošiljanje sporočila
var sendMessage = function () {
    $.ajax({
      url: baseUrl1 + "/messages/" + currentRoom,
      type: "POST",
      contentType: 'application/json',
      data: JSON.stringify({user: user, text: $("#message").val()}),
      success: function (data) {
        $("#message").val("");
      },
      error: function(err) {
        alert("Prišlo je do napake pri pošiljanju sporočila. Prosimo poskusite znova!")
      }
    });
  };
  
  // On Click handler za pošiljanje sporočila
  $("#send").click(sendMessage);
  


  // TODO: Naloga
  // Definicija funkcije za menjavo sobe (izbriši pogovore in uporabnike na strani, nastavi spremenljivko currentRoom, nastavi spremenljivko nextMessageId na 0)
  // V razmislek: pomisli o morebitnih težavah!
  
  var changeRoom = function (event) {
    $("#messages").html("");
    $("#users").html("");
    currentRoom = event.currentTarget.getElementsByTagName("h5")[0].innerHTML;
    nextMessageId = 0;
  };


  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);
  });


	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});


	$('#preberiObstojeciVitalniZnak').change(function() {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("");
		var podatki = $(this).val().split("|");
		$("#dodajVitalnoEHR").val(podatki[0]);
		$("#dodajVitalnoDatumInUra").val(podatki[1]);
		$("#dodajVitalnoTelesnaVisina").val(podatki[2]);
		$("#dodajVitalnoTelesnaTeza").val(podatki[3]);
		$("#dodajVitalnoMerilec").val(podatki[4]);
	});

  /**
   * Napolni testni EHR ID pri pregledu meritev vitalnih znakov obstoječega
   * bolnika, ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Ata Smrk, Pujsa Pepa)
   */
	$('#preberiEhrIdZaVitalneZnake').change(function() {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("");
		$("#rezultatMeritveVitalnihZnakov").html("");
		$("#meritveVitalnihZnakovEHRid").val($(this).val());
	});

});



